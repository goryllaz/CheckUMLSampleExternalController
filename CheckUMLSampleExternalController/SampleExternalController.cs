﻿using System.Collections.Generic;
using CheckUMLAlerts;
using CheckUMLExternalController;
using EA;

namespace CheckUMLSampleExternalController
{
    public class SampleExternalController: IEAExternalController
    {
        public List<IAlert> GetResults()
        {
            List<IAlert> alerts = new List<IAlert>();
            for (short i = 0; i < Repository.Models.Count; i++)
            {
                Package model = (Package) Repository.Models.GetAt(i);
                if (model.Name != "Model")
                    alerts.Add(new Error($"(Model: {model.Name}) - Název kořenového adresáře musí být Model"));
            }

            return alerts;
        }

        public Repository Repository { get; set; }
    }
}
